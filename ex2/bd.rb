require './projeto.rb'
require './funcionario.rb'
require './dependente.rb'

class Bd 
    @@projetos = []
    @@departamentos = []
    @@funcionarios = []
    @@dependentes = []
    
    def self.adiciona_projetos vetor, objeto
        case vetor
        when "projetos"
          @@projetos.append(objeto)
        end
    end

    def self.adiciona_departamentos vetor, objeto
        case vetor
        when "departamentos"
          @@departamentos.append(objeto)
        end
    end
    def self.adiciona_funcionarios vetor, objeto
        case vetor
        when "funcionarios"
          @@funcionarios.append(objeto)
        end
    end

    def self.adiciona_dependentes vetor, objeto
        case vetor
        when "dependentes"
          @@dependentes.append(objeto)
        end
    end

    
end
