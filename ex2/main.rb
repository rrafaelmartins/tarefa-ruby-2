require './bd.rb'
require 'faker'

project = Projeto.new({
    nome: 'projeto teste',
    fundos: Faker::Number.between(from: 1000, to: 10000),
    nome_cliente: Faker::Name.first_name,
    departamento: 'Gestão de Pessoas'
})

def cadastra_funcionario (project)
    Bd.adiciona_projects "projects", project
    Bd.adiciona_departamentos "departamentos", project.departamento
    puts "Departamento #{project.departamento}:"
    puts "Projeto #{project.nome} de #{project.nome_cliente} cadastrado." 
end
cadastra_funcionario(project)



funcionario = Funcionario.new({
    primeiro_nome: Faker::Name.first_name, 
    segundo_nome: Faker::Name.middle_name,
    idade: Faker::Number.between(from: 18, to: 60),
    salario: Faker::Number.between(from: 2000, to: 6000),
    endereco: Faker::Address.street_address,
    dependentes: Dependente.new({
       nome: Faker::Name.first_name,
       idade: Faker::Number.between(from: 18, to: 60),
       parentesco: 'parente'
    }),
    setor: Faker::Appliance.brand,
    matricula: Faker::Number.number(digits: 4)
})

def cadastra_funcionario (funcionario)
    Bd.adiciona_funcionarios "funcionarios", funcionario
    puts "#{funcionario.primeiro_nome} #{funcionario.segundo_nome} cadastrado. " 
    Bd.adiciona_dependentes "dependentes", funcionario.dependentes
    puts "#{funcionario.primeiro_nome} cadastrou seu parente #{funcionario.dependentes.nome}" 
end
cadastra_funcionario(funcionario)


